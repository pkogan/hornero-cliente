export interface EntradaJuego {
    parametrosEntrada:string;
    token:string;
}

export interface RespuestaJuego {
    respuesta:string;
    token:string;
}