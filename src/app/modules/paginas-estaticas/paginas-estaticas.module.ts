import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaginasEstaticasRoutingModule } from './paginas-estaticas-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PaginasEstaticasRoutingModule
  ]
})
export class PaginasEstaticasModule { }
